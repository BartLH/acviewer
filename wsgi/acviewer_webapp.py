from bottle import (debug, default_app, redirect, request, response, route,
                    template, TEMPLATE_PATH)
import os
import re
from datetime import datetime as dt
from datetime import timedelta as td

debug(True)

from ACViewer import AntCityGame


game = None
last = '/'

def needs_login(fn):
    def check_cookies(**kwargs):
        username = request.get_cookie('username')
        password = request.get_cookie('password')

        if username and password:
            global game
            if not game:
                game = AntCityGame(username, password)
            return fn(**kwargs)
        else:
            global last
            last = request.path
            redirect("/login")

    return check_cookies


@route('/')
def index():
    return '''<ul><li><a href="/hospital">Hospital</a></li>
        <li><a href="/empire">Players in empire</a></li>
        <li><a href="/emp_in_hos">Empire members in hospital</a></li></ul>'''

@route('/topants')
@needs_login
def top():
    page = game.urlopen('halloffame.php?action=total')
    res = re.findall(r"<tr> <td>(?:<b>)?(\d+)(?:</b>)?</td> <td><a href='viewuser\.php\?u=(\d+)'>(?:<b>)?(.+?)(?:</b>)?</a>", page)[:50]
    game.get_details(map(int, (i[1] for i in res)))
    if request.GET.get('BB') == 'True':
        out = '<a href="?BB=False"><input type="button" value="BB-code off" /></a><br />\n'
    else:
        out = '<a href="?BB=True"><input type="button" value="BB-code" /></a><br />\n'
    for pos, id, name in res:
        p = game.players[int(id)]
        if request.GET.get('BB') == 'True':
            templ = '[size=5]%s - [url=viewuser.php?u=%s]%s[/url][/size]<br />\nID: %s | level: %s | empire: %s %s<br />\nPrev:  | Weeks On:  | Peak: \n<br />\n<br />\n'
        else:
            templ = '<p><font size="5">%s - <a href="http://play.antcitygame.com/viewuser.php?u=%s">%s</a></font><br />\nID: %s | level: %s | empire: %s %s<br />\n</p>'
        out += templ % (pos, id, p.name, id, p.level, name.replace(p.name, '').strip(), p.empire)
    return out

@route('/topactive')
@needs_login
def topact():
    page = game.urlopen('halloffame.php?action=total')
    res = re.findall(r"<tr> <td>(?:<b>)?(\d+)(?:</b>)?</td> <td><a href='viewuser\.php\?u=(\d+)'>(?:<b>)?(.+?)(?:</b>)?</a>", page)
#    game.get_details(map(int, (i[1] for i in res)))
    if request.GET.get('BB') == 'True':
        out = '<a href="?BB=False"><input type="button" value="BB-code off" /></a><br />\n'
    else:
        out = '<a href="?BB=True"><input type="button" value="BB-code" /></a><br />\n'
    i = 0
    delta = td(7)
    now = dt.now()
    for pos, id, name in res:
        if i == 20:
            break
        game.get_details([int(id)])
        p = game.players[int(id)]
        date = dt.strptime(p.lastact, '%B %d, %Y %I:%M:%S %p')
        if now - date > delta:
            continue
        if request.GET.get('BB') == 'True':
            templ = '[size=5]%s - [url=viewuser.php?u=%s]%s[/url][/size]<br />\nID: %s | level: %s | empire: %s %s<br />\nPrev:  | Weeks On:  | Peak: \n<br />\n<br />\n'
        else:
            templ = '<p><font size="5">%s - <a href="http://play.antcitygame.com/viewuser.php?u=%s">%s</a></font><br />\nID: %s | level: %s | empire: %s %s<br />\n</p>'
        out += templ % (i + 1, id, p.name, id, p.level, name.replace(p.name, '').strip(), p.empire)
        i += 1
    return out


@route('/date')
def date():
    return str(dt.now())


@route('/id/<id:int>')
def id(id):
    return template('view_id', id=id)

@route('/empire')
def empire():
    if request.GET.get('empire'):
        redirect('/empire/%s' % request.GET.get('empire'))
    else:
        return '''<form action="/empire">
            <input type="text" name="empire" />
            <input type="submit" />
            </form>'''

@route('/empire/<empire:int>')
@needs_login
def players(empire):
    players = game.ants_in_empire(empire)
    game.get_details(players)
    rows = make_rows(players)
    return template('make_table', rows=rows)

@route('/hospital')
@needs_login
def hospital():
    players = game.ants_in_hospital()
    game.get_details(players)
    rows = make_rows(players, show_empire=True)
    return template('make_table', rows=rows)

@route('/emp_in_hos')
def emp_in_hos_pre():
    if request.GET.get('empire'):
        redirect('/emp_in_hos/%s' % request.GET.get('empire'))
    else:
        return '''<form action="/emp_in_hos">
            <input type="text" name="empire" />
            <input type="submit" />
            </form>'''

@route('/emp_in_hos/<empire:int>')
@needs_login
def emp_in_hos(empire):
    in_emp = game.ants_in_empire(empire)
    game.get_details(in_emp)
    in_hosp = game.ants_in_hospital()

    out = '<h2>IN HOSPITAL:</h2>\n'
    rows = make_rows([id for id in in_emp if id in in_hosp])
    out += template('make_table', rows=rows)
    out += '\n<h2>NOT IN HOSPITAL:</h2>\n'
    rows = make_rows([id for id in in_emp if id not in in_hosp])
    out += template('make_table', rows=rows)
    return out

@route('/login')
def login():
    return template('login')

@route('/auth', method=['GET', 'POST'])
def auth():
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    if not (username and password):
        return 'Fill in the form correctly!'
    response.set_cookie('username', username)
    response.set_cookie('password', password)
    redirect(last)


def make_rows(players, show_empire=False):
    rows = [['Name', 'Level', 'Strength', 'Agility', 'Guard']]
    if show_empire: rows[0].append('Empire')
    for id in players:
        p = game.players[id]
        row = [p.name, p.level, p.strength, p.agility, p.guard]
        if show_empire: row.append(p.empire)
        rows.append(row)
    return rows


TEMPLATE_PATH.append(os.path.join(os.environ['OPENSHIFT_HOMEDIR'], 'app-root/runtime/repo/wsgi/views'))
TEMPLATE_PATH.append('acviewer/wsgi/views')
TEMPLATE_PATH.append('wsgi/views')

application=default_app()


if __name__ == '__main__':
    from bottle import run
    run(host=os.environ['IP'], port=int(os.environ['PORT']), reloader=True, debug=True)