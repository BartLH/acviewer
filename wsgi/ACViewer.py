from collections import defaultdict
from operator import itemgetter
import cookielib
import urllib
import urllib2
import re


class AntCityGame(object):
    base = 'http://play.antcitygame.com/'

    def __init__(self, username, password, player_level='auto'):
        self.players = defaultdict(lambda: Player(self))
        self.data = urllib.urlencode({'username': username, 'password': password})
        cookies = cookielib.CookieJar()
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(cookies))
        self.login()
        if player_level == 'auto':
            print 'Getting player level...',
            page = self.urlopen('')
            self.player_level = int(re.search(r'<div class="exp">\s+<font size="1"> LV\.(\d+)</font>', page, re.MULTILINE).group(1))
            print 'level %d set. [Done]' % self.player_level
        else:
            self.player_level = player_level

    def urlopen(self, urlpart, data=None):
        return self.opener.open(self.base + urlpart, data).read()

    def login(self):
        print 'Logging in...',
        page = self.urlopen('authenticate.php', self.data)
        print '[Done]'

    def update_infos(self, info):
        for id, values in info.iteritems():
            self.players[id].__dict__.update(values)

    def view_empires(self):
        page = self.urlopen('empirelist.php')
        emps = re.findall(r"href='gangs\.php\?action=view&ID=(\d+)'>(.*?)</a>.+</a></td>\s*?<td align='center'>(\d+)</td>",
                          page, re.MULTILINE)
        max_id = max(2, max(len(id) for id, name, resp in emps + [('', '', '')]))  # plus [('', '', '')] in case there are no entries
        max_name = max(4, max(len(name) for id, name, resp in emps + [('', '', '')]))
        print '{0:<{1}} {2:<{3}} Respect'.format('ID', max_id, 'Name', max_name)
        print '\n'.join('{0:<{1}} {2:<{3}} {4}'.format(id, max_id, name, max_name, respect)
                for id, name, respect in sorted(emps, key=lambda x: x[1].lower()))

    def ants_in_hospital(self):
        print 'Getting ants in hospital...',
        page = self.urlopen('hospital.php')
        names = re.findall(r"<td width='40%'> <a href='viewuser\.php\?u=(\d+)'>(.+?)</a>", page)
        self.update_infos(dict((int(id), {'name': name}) for id, name in names))
        print '[Done]'
        return [int(id) for id, name in names]

    def ants_in_empire(self, id):
        print 'Getting ants in empire %d...' % id,
        page = self.urlopen('gangs.php?action=userlist&ID=%s' % id)
        empire = re.search(r"<h3>Userlist for (.+?)</h3>", page).group(1)
        names = re.findall(r"<tr><td><a href='viewuser\.php\?u=(\d+)'>(.+?)</a>", page)
        self.update_infos(dict((int(id), {'name': name, 'empire': empire}) for id, name in names))
        print '[Done]'
        return [int(id) for id, name in names]

    def get_details(self, players):
        print 'Getting details...',
        for id in players:
            p = self.players[id]
            page = self.urlopen('viewuser.php?u=%s' % id)
            if not p.name:
                p.name = re.search(r"<span style='font-size: 29px; color:yellow';><b><u>(.+?)</u></b></span>",
                                   page).group(1)
            print p.name,
            if not p.empire:
                p.empire = re.search(r"Empire:</td><td align='left'>(<a.+?>)?<font color='#.+?'>(.+?)<",
                                     page).group(2)
            p.level = int(re.search(r"LVL:</span> <b>(\d+)<", page).group(1))
            for stat in ('Strength', 'Agility', 'Guard'):
                value = re.search(r"<td align='left'>%s</td>\s*<td align='left'>(\d+)</td>"
                                  % stat, page, re.MULTILINE).group(1)
                setattr(p, stat.lower(), int(value))
            p.lastact = re.search(r"<tr><td align='left'>Last Active:</td><td align='left'>(.+?)</td></tr>",
                                  page).group(1)
        print '[Done]'

    def format_players(self, players, show_emp=False):
        if not players:
            return '-'
        max_name = max(len(self.players[id].name) for id in players) + 3
        # There has to be a better way than repeating this for every stat...
        # Minimum length is 3
        max_lvl = max(3, max(len(str(self.players[id].level)) for id in players))
            # We need a max_lvl var in the case it is None
        max_str = max(3, max(len(str(self.players[id].strength)) for id in players))
        max_agi = max(3, max(len(str(self.players[id].agility)) for id in players))
        max_gua = max(3, max(len(str(self.players[id].guard)) for id in players))

        out = '{0} {1:>{2}} {3:>{4}} {5:>{6}} {7:>{8}}'.format(' ' * max_name, 'lvl', max_lvl, 'str', max_str,
                                                               'agi', max_agi, 'gua', max_gua)
        for id in players:
            player = self.players[id]
            out += '\n'
#            out += '=> ' * player.is_in_range()
            out += '{name:{0}} {p.level:>{1}} {p.strength:>{2}} {p.agility:>{3}} {p.guard:>{4}}'\
                    .format(max_name, max_lvl, max_str, max_agi, max_gua,
                            name=('=> ' * player.is_in_range() + player.name), p=player)
            if show_emp and player.empire:
                out += ' - ' + player.empire
        return out

    def empire_in_hospital(self, empire_id):
        in_emp = self.ants_in_empire(empire_id)
        self.get_details(in_emp)
        in_hosp = self.ants_in_hospital()

        print 'IN HOSPITAL:'
        print self.format_players([id for id in in_emp if id in in_hosp])
        print
        print 'NOT IN HOSPITAL:'
        print self.format_players([id for id in in_emp if id not in in_hosp])


class Player(object):
    def __init__(self, game, name=None, empire=None, level=None, strength=None, agility=None, guard=None):
        self.game = game
        self.name = name
        self.level = level
        self.empire = empire
        self.strength = strength
        self.agility = agility
        self.guard = guard

    def __setattr__(self, name, value):
        if value == 'None': value = None
        object.__setattr__(self, name, value)

    def __str__(self):
        return '\n'.join(('%s: %s' % (attr, getattr(self, attr)) for attr in
                         ('name', 'level', 'empire', 'strength', 'agility', 'guard')))

    def is_in_range(self):
        return (self.game.player_level - 5 <= self.level <= self.game.player_level + 5)
