%#template to generate a HTML table from a list of tuples (or list of lists, or tuple of tuples or ...)
<table border="1">
  <tr>
  %for col in rows[0]:
    <th>{{col}}</th>
  %end
  </tr>
%for row in rows[1:]:
  <tr>
  %for col in row:
    <td>{{col}}</td>
  %end
  </tr>
%end
</table>