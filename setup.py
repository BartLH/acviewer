from setuptools import setup

setup(name='ACViewer',
      version='0.1',
      description='Viewer for Ant City',
      author='BrtH',
      author_email='brthttm@gmail.com',
      url='www.antcitygame.com',
      install_requires=['bottle'],
     )
